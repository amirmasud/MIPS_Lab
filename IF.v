module IF_stage
(
	input 				clk, rst,
	input				instr_fetch_enable,
	input [5:0] 		imm_branch_offset,
	input 				branch_enable,
	output reg [7:0] 	pc,
	output reg [7:0]  id_pc,
	output [15:0]		instr
);
	Inst_mem	Inst_mem_inst (
	.address ( pc ),
	.clken ( instr_fetch_enable ),
	.clock ( clk ),
	.q ( instr )
	);


	always@(posedge clk, posedge rst)
	begin
		if(rst) begin
			pc <= 8'b0;
			id_pc <= 0;
		end
		else begin
			if(instr_fetch_enable) begin
				id_pc <= pc;
				if(branch_enable)
					pc <= pc + $signed(imm_branch_offset[5:0]); //2{imm_branch_offset[5]}},
				else
						pc <= pc + 8'b1;
			end
		end
	end


endmodule
