module Forwarding_Unit (input [2:0] id_src1, id_src2,
                        input id_op_code_is_st,
                        input [2:0] ex_op_dest, mem_op_dest,
                        output reg [1:0] frwd_op1_mux, frwd_op2_mux, frwd_store_data);
parameter FORWARD_EX_RES = 2'b10;
parameter FORWARD_MEM_RES = 2'b11;

        always@(*) begin
          {frwd_op1_mux, frwd_op2_mux, frwd_store_data} <= 0;

          if(id_src1 != 0) begin
            if(id_src1 == ex_op_dest)
              frwd_op1_mux <= FORWARD_EX_RES;
            else if (id_src1 == mem_op_dest) // && (id_src2 != ex_op_dest  || id_src2 == 0) what the hell?
              // &&...=> used to avoid forwarding from mem if already forwarding from EX
              frwd_op1_mux <= FORWARD_MEM_RES;
          end

          if(id_src2 != 0) begin
            if(id_op_code_is_st) begin
              if(id_src2 == ex_op_dest)
                frwd_store_data <= FORWARD_EX_RES;
              else if (id_src2 == mem_op_dest)
                frwd_store_data <= FORWARD_MEM_RES;
            end else begin
              if(id_src2 == ex_op_dest)
                frwd_op2_mux <= FORWARD_EX_RES;
              else if (id_src2 == mem_op_dest) // && (id_src1 != ex_op_dest || id_src1 == 0) what the hell?
                frwd_op2_mux <= FORWARD_MEM_RES;
            end

          end

        end
endmodule
