module Cache_controller(
    input clk, rst,
    input [15:0] mem_addr,
    input [15:0] write_data,
    input write_en,
    input mem_op,
    output reg [15:0] output_data,
    output reg ready,
    // to off-chip SRAM
    output [17:0] SRAM_ADDR,
  	inout  [15:0] SRAM_DQ,
    output SRAM_WE_N,
  	output SRAM_OE_N,
  	output SRAM_UB_N,
  	output SRAM_LB_N,
  	output SRAM_CE_N
  );
  parameter DATA_WIDTH = 16;
  parameter CACHE_SIZE = 256;
  // parameter CACHE_SIZE_WIDTH = 8; //bit
  parameter IDX_WIDTH = 8;
  parameter TAG_WIDTH = 8;
  parameter WAY1 = 1'b0, WAY2 = 1'b1;
  parameter LRU_OLD = 1'b0, LRU_NEW =1'b1;


  SRAM_controller data_mem(clk, rst,
    mem_addr, write_data, write_en,
    mem_op, cache_miss, sram_data, sram_ready,
    SRAM_ADDR, SRAM_DQ,
    SRAM_WE_N, SRAM_OE_N, SRAM_UB_N, SRAM_LB_N, SRAM_CE_N);

  //cache
  reg [DATA_WIDTH-1:0] cache_data1 [0:CACHE_SIZE-1];
  reg [DATA_WIDTH-1:0] cache_data2 [0:CACHE_SIZE-1];
  reg [TAG_WIDTH-1:0] cache_tag1 [0:CACHE_SIZE-1];
  reg [TAG_WIDTH-1:0] cache_tag2 [0:CACHE_SIZE-1];
  reg cache_valid1 [0:CACHE_SIZE-1];
  reg cache_valid2 [0:CACHE_SIZE-1];
  reg cache_LRU1 [0:CACHE_SIZE-1];
  reg cache_LRU2 [0:CACHE_SIZE-1];

  //wire cache_hit = !cache_miss;
  wire [IDX_WIDTH-1:0] index = mem_addr[7:0];
  wire [TAG_WIDTH-1:0] tag = mem_addr[15:8];

  wire [DATA_WIDTH-1:0] sram_data;
  wire sram_ready;
  reg cache_miss;
  wire [15:0] immidiate_data;
/*
  function read_cache(input current_way, input other_way);
  begin
      if (cache_tag[index][current_way] == tag && cache_valid[index][current_way] == 1)
      begin
        output_data = cache_data[index][current_way];
        cache_LRU[index][current_way] = LRU_NEW;
        cache_LRU[index][other_way] = LRU_OLD;
        ready = 1;
        read_cache = 1;
      end
      else read_cache = 0;
  end
  endfunction

  task update_cache_way(input way, input other_way, input [TAG_WIDTH-1:0] tag,input [15:0] output_data);
  begin
    cache_valid[index][way] <= 1;
    cache_data[index][way] <= output_data;
    cache_tag[index][way] <= tag;
    cache_LRU[index][way] <= LRU_NEW;
    cache_LRU[index][other_way] <= LRU_OLD;
  end
  endtask
*/

  always @(*) begin
    {ready, cache_miss, output_data} = 0;
    if(mem_op) begin
      if (!write_en ) begin  // LOAD LD
        if (cache_tag1[index] == tag && cache_valid1[index] == 1)
        begin
          output_data = cache_data1[index];
          ready = 1;
        end
        else if (cache_tag2[index] == tag && cache_valid2[index] == 1)
        begin
          output_data = cache_data2[index];
          ready = 1;
        end
        else begin
            ready = sram_ready;
            output_data = sram_data;
            cache_miss = 1;
        end
      end
      else begin //write_en -- STORE
        if(sram_ready)
          ready = 1;
      end

    end
  end

  integer i, j;
  always @(posedge clk) begin
    if(rst) begin
      for(i= 0; i< CACHE_SIZE ; i= i+1)begin //256
        cache_valid1[i] <= 0;
			  cache_valid2[i] <= 0;
		  end
    end
    if (write_en && sram_ready) begin
      // if it is already in the cache
      if (cache_tag1[index] == tag && cache_valid1[index] == 1)
        cache_data1[index] <= write_data;
      else if (cache_tag2[index] == tag && cache_valid2[index] == 1)
        cache_data2[index] <= write_data;
      else begin // write new data to cache if it is not already in cache
        if(cache_LRU1[index]== LRU_OLD) begin
          cache_valid1[index]<= 1;
          cache_data1[index] <= write_data;
          cache_tag1[index]<= tag;
          cache_LRU1[index] <= LRU_NEW;
          cache_LRU2[index] <= LRU_OLD;
        end
        else begin //cache_LRU2[index]== LRU_OLD
          cache_valid2[index] <= 1;
          cache_data2[index] <= write_data;
          cache_tag2[index] <= tag;
          cache_LRU2[index] <= LRU_NEW;
          cache_LRU1[index]<= LRU_OLD;
        end
      end
    end
    else if(!write_en) begin
      if (cache_tag1[index] == tag && cache_valid1[index] == 1) begin
        cache_LRU1[index] <= LRU_NEW;
        cache_LRU2[index] <= LRU_OLD;
      end
      else if (cache_tag2[index] == tag && cache_valid2[index] == 1) begin
        cache_LRU2[index] <= LRU_NEW;
        cache_LRU1[index] <= LRU_OLD;
      end

      //cache update in case of cache miss
      if(ready && cache_miss)begin
        if(cache_LRU1[index]== LRU_OLD) begin
          cache_valid1[index]<= 1;
          cache_data1[index] <= output_data;
          cache_tag1[index]<= tag;
          cache_LRU1[index] <= LRU_NEW;
          cache_LRU2[index] <= LRU_OLD;
        end
        else begin //cache_LRU2[index]== LRU_OLD
          cache_valid2[index] <= 1;
          cache_data2[index] <= output_data;
          cache_tag2[index] <= tag;
          cache_LRU2[index] <= LRU_NEW;
          cache_LRU1[index]<= LRU_OLD;
        end
      end
      //else begin @TODO: write to cache (write allocate)
      // if(write_en, sram_ready)
      //   insert_to_cache(mem_addr, write_data);
    end
  end

endmodule
