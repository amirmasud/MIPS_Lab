module MEM_stage
(
  input clk,
  input rst,

  input [7:0] mem_pc,
  output reg [7:0] wb_pc,

  // to off-chip memory
  output [17:0] SRAM_ADDR,
	inout [15:0] SRAM_DQ,
  output SRAM_WE_N,
	output SRAM_OE_N,
	output SRAM_UB_N,
	output SRAM_LB_N,
	output SRAM_CE_N,
  //from EX stage
  input ex_op_code_is_mem,
  input[2:0] ex_op_dest,
  input ex_wb_mux,
  input ex_wb_en,
  input [15:0] ex_alu_res,
  input [15:0] ex_mem_store_data,
  input ex_mem_write_enable,
  //to WB Stage
  output reg mem_wb_mux,
  output reg mem_wb_en,
  output reg [2:0] mem_op_dest,
  output reg [15:0] mem_alu_res,
  output reg [15:0] mem_mem_data,
  // from Hazard_detection
  input freeze,
  // to Hazard_detection
  output ready
);

wire [15:0] mem_addr;
assign mem_addr = ex_alu_res;

wire [15:0] not_ready_data;

Cache_controller cache_cntrl(clk, rst,
  mem_addr, ex_mem_store_data, ex_mem_write_enable,
  ex_op_code_is_mem, not_ready_data, ready,
  SRAM_ADDR, SRAM_DQ,
  SRAM_WE_N, SRAM_OE_N, SRAM_UB_N, SRAM_LB_N, SRAM_CE_N
  );
//
// data_mem	data_mem_inst (
// 	.address ( mem_addr ),
// 	.clock ( clk ),
// 	.data ( ex_mem_store_data ),
// 	.wren ( ex_mem_write_enable ),
// 	.q ( mem_mem_data )
// 	);



	always@(posedge clk, posedge rst)
	begin
		if(rst)
			{wb_pc, mem_wb_en, mem_wb_mux, mem_op_dest, mem_mem_data} <= 0;
		else begin
      if (!freeze) begin
        wb_pc <= mem_pc;
  			//write back controll signals
  			mem_wb_mux <= ex_wb_mux;
  			mem_wb_en <= ex_wb_en;
  			mem_op_dest <= ex_op_dest;
  			mem_alu_res <= ex_alu_res;
        mem_mem_data <= not_ready_data;
      end
		end

	end




endmodule
