module reg_file
(
  input clk,
  input rst,

  input freeze,

  input rg_wrt_en,
  input [2:0] rg_wrt_dest,
  input [15:0] rg_wrt_data,

  input [2:0] rg_rd_addr1,
  output[15:0] rg_rd_data1,

  input [2:0] rg_rd_addr2,
  output[15:0] rg_rd_data2
);

	reg [15:0] R [7:0];

	assign rg_rd_data1 =  R[rg_rd_addr1];
	assign rg_rd_data2 =  R[rg_rd_addr2];

	integer i;

	always@(negedge clk)
	begin
		if(rst) begin
			for(i = 0; i<= 7; i = i+1)
			begin
				R[i] <= 0;
			end
		end else begin
			if (rg_wrt_en && rg_wrt_dest != 0) //&& !freeze
				R[rg_wrt_dest] <= rg_wrt_data;
		end
	end


endmodule
