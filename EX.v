module EX_stage
(
  input clk,
  input rst,

  input [7:0] ex_pc,
  output reg [7:0] mem_pc,

  input freeze,

  //from ID stage
  input [2:0] alu_cmd,
  input [2:0] id_op_dest,
  input [15:0] operand1,
  input [15:0] operand2,
  input [15:0] id_mem_store_data,
  input [15:0] frwd_res_ex,
  input [15:0] frwd_res_mem,
  input [1:0] frwd_op1_mux,
  input [1:0] frwd_op2_mux,
  input [1:0] frwd_store_data,
  input id_wb_mux,
  input id_wb_en,
  input id_mem_write_enable,
  input id_op_code_is_ld,
  input id_op_code_is_st,
  //to MEM stage
  output reg ex_op_code_is_mem,
  output reg ex_wb_mux,
  output reg ex_wb_en,
  output reg [2:0] ex_op_dest,
  output reg [15:0] ex_alu_res,
  output reg [15:0] ex_mem_store_data,
  output reg ex_mem_write_enable
);
  parameter FORWARD_EX_RES = 2'b10;
  parameter FORWARD_MEM_RES = 2'b11;
  wire [15:0] alu_op1, alu_op2;
  assign alu_op1 =(frwd_op1_mux == FORWARD_EX_RES) ? frwd_res_ex :
                  ((frwd_op1_mux == FORWARD_MEM_RES) ? frwd_res_mem : operand1);
  assign alu_op2 =(frwd_op2_mux == FORWARD_EX_RES) ? frwd_res_ex :
                  ((frwd_op2_mux == FORWARD_MEM_RES) ? frwd_res_mem : operand2);

	wire [15:0] alu_res;
	ALU alu(alu_op1, alu_op2, alu_cmd, alu_res);

	always@(posedge clk, posedge rst)
	begin
		if(rst) begin
			ex_alu_res <= 0;
      mem_pc <= 0;
			{ex_op_code_is_mem, ex_wb_en, ex_wb_mux, ex_op_dest, ex_mem_store_data, ex_mem_write_enable} <= 0;
		end
		else begin
      if(!freeze) begin
        mem_pc <= ex_pc;
        ex_op_code_is_mem <= (id_op_code_is_ld || id_op_code_is_st);
        ex_mem_write_enable <= id_mem_write_enable;
        ex_mem_store_data <= (frwd_store_data== FORWARD_EX_RES) ? frwd_res_ex:
                              (frwd_store_data== FORWARD_MEM_RES) ? frwd_res_mem : id_mem_store_data;
  			ex_alu_res <= alu_res;
  			//write back controll signals
  			ex_wb_mux <= id_wb_mux;
  			ex_wb_en <= id_wb_en;
  			ex_op_dest <= id_op_dest;
      end
		end
	end


endmodule
