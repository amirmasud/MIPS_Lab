module ALU
(
  input [15:0] a,
  input [15:0] b,
  input [2:0] cmd,

  output reg[15:0] r
);

	always@(*) begin
		case (cmd)
			3'b000: r = a + b;
			3'b001: r = a - b;
			3'b010: r = a & b;
			3'b011: r = a | b;
			3'b100: r = a ^ b;
			3'b101: r = a << b;
			3'b110: r = a >> b;
			3'b111: r = a >>> b;
			default: r = a + b;
		endcase
	end

endmodule
