//`default_nettype none //turn off implicit data types
module MIPS
(
	input CLOCK_50,
	input [17:0] SW, // SW0 => rst
	output [17:0] SRAM_ADDR,
	inout [15:0] SRAM_DQ,
	output SRAM_WE_N,
	output SRAM_OE_N,
	output SRAM_UB_N,
	output SRAM_LB_N,
	output SRAM_CE_N
	//output [6:0] HEX0, HEX1, HEX2, HEX3 //7SEGs
);
	wire [15:0] instr;
	MIPS_DP mips_dp (CLOCK_50,
		SW[0], SRAM_ADDR, SRAM_DQ,
		SRAM_WE_N, SRAM_OE_N, SRAM_UB_N, SRAM_LB_N, SRAM_CE_N,
		 instr);
endmodule
