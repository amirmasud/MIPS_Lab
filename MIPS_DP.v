module MIPS_DP
(
	input	clk, rst,
	output [17:0] SRAM_ADDR,
	inout [15:0] SRAM_DQ,
	output SRAM_WE_N,
	output SRAM_OE_N,
	output SRAM_UB_N,
	output SRAM_LB_N,
	output SRAM_CE_N,
	output 	[15:0] 	instr
);
	wire instr_fetch_enable, branch_enable, stall, freeze;
	wire [7:0] if_pc, id_pc, ex_pc, mem_pc, wb_pc;
	assign instr_fetch_enable = !stall && !freeze;

	wire instruction_decode_en;
	assign instruction_decode_en = !stall;

	wire[2:0] src1, src2, id_dest, alu_cmd;
	wire[15:0] reg_read_data1, reg_read_data2, operand1, operand2,
	 					ex_alu_res, mem_alu_res, wb_write_data, id_mem_store_data;

	wire [15:0] frwd_res_ex, frwd_res_mem;
	wire [1:0] frwd_op1_mux, frwd_op2_mux, frwd_store_data;
	wire [2:0] id_src1, id_src2; // src addr in ID/EX regs

	wire [5:0] branch_offset_imm;

	wire id_wb_mux, id_wb_en, ex_wb_mux, ex_wb_en, mem_wb_mux, mem_wb_en, wb_write_en;

	wire [15:0] ex_mem_store_data, mem_mem_data;
	wire id_mem_write_enable, ex_mem_write_enable;

	wire [2:0] id_op_dest, ex_op_dest, mem_op_dest, wb_write_dest;

	wire id_op_code_is_ld, id_op_code_is_st, ex_op_code_is_mem;

	wire ready;

	IF_stage IF(clk, rst, instr_fetch_enable, branch_offset_imm, branch_enable, if_pc, id_pc, instr);

	ID_stage ID(clk,rst, id_pc, ex_pc, instruction_decode_en, freeze, instr /*instruction*/,
		 branch_offset_imm, branch_enable , reg_read_data1, reg_read_data2,
		 alu_cmd, id_dest, src1, src2, operand1, operand2, id_mem_store_data, id_mem_write_enable,
		 id_wb_mux, id_wb_en,  id_op_code_is_ld, id_op_code_is_st, id_src1, id_src2);

	EX_stage EX(clk, rst,  ex_pc, mem_pc, freeze, alu_cmd, id_dest, operand1, operand2, id_mem_store_data,
		frwd_res_ex,frwd_res_mem,frwd_op1_mux, frwd_op2_mux, frwd_store_data,
		id_wb_mux, id_wb_en, id_mem_write_enable, id_op_code_is_ld, id_op_code_is_st,
		ex_op_code_is_mem,
		ex_wb_mux, ex_wb_en,ex_op_dest, ex_alu_res, ex_mem_store_data, ex_mem_write_enable);

	assign frwd_res_ex = ex_alu_res;
	MEM_stage MEM(clk, rst, mem_pc, wb_pc,
		SRAM_ADDR, SRAM_DQ, SRAM_WE_N, SRAM_OE_N, SRAM_UB_N, SRAM_LB_N, SRAM_CE_N,
		ex_op_code_is_mem,
		ex_op_dest, ex_wb_mux, ex_wb_en, ex_alu_res,
		ex_mem_store_data, ex_mem_write_enable, mem_wb_mux, mem_wb_en, mem_op_dest,
		mem_alu_res, mem_mem_data, freeze, ready);

	assign frwd_res_mem = wb_write_data;
	WB_stage WB(mem_wb_mux, mem_wb_en, mem_op_dest, mem_alu_res,
		mem_mem_data, wb_write_en, wb_write_dest, wb_write_data);


	reg_file Rfile(clk,rst, freeze, wb_write_en, wb_write_dest, wb_write_data, src1, reg_read_data1, src2, reg_read_data2);

	Hazard_detection HDU(src1, src2, id_dest, ex_op_dest, mem_op_dest,
		 									id_op_code_is_ld, ex_op_code_is_mem, ready,
											stall, freeze);
	Forwarding_Unit FU (id_src1, id_src2, id_op_code_is_st, ex_op_dest, mem_op_dest,
											frwd_op1_mux, frwd_op2_mux, frwd_store_data);
endmodule
