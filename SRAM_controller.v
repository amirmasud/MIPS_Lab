module SRAM_controller(
  input clk, rst,
  //from EX stage
  input [15:0] mem_addr,
  input [15:0] store_data,
  input write_enable,
  input mem_op,
  input cache_miss,
  // to WB stage
  output [15:0] mem_data,
  output reg ready,
  // to off-chip SRAM
  output reg [17:0] SRAM_ADDR,
	inout  reg [15:0] SRAM_DQ,
  output reg SRAM_WE_N,
	output SRAM_OE_N,
	output SRAM_UB_N,
	output SRAM_LB_N,
	output SRAM_CE_N
  );

  //chip enable and ...
  assign SRAM_OE_N = 0, SRAM_UB_N = 0, SRAM_LB_N = 0, SRAM_CE_N = 0;
  assign mem_data = SRAM_DQ;


  reg [1:0] counter;
  always@(posedge clk) begin
    if(mem_op && (cache_miss|| write_enable))
      counter <= counter + 2'b1;
    else
      counter <= 0;

    if(counter == 2) begin
      ready <= 1;
      // counter <= 0;
    end else
      ready <= 0;
	end
  always @ ( * ) begin
    SRAM_ADDR <= {2'b0,mem_addr};
    SRAM_WE_N <= ~ write_enable; // active low
    SRAM_DQ <= write_enable ? store_data : 16'bz;
  end

endmodule
