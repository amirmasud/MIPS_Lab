module ID_stage
(
  input clk,
  input rst,

  input [7:0] id_pc,
  output reg [7:0] ex_pc,

  input instruction_decode_en,
  input freeze,
  input [15:0] instruction,
  output [5:0] branch_offset_imm,
  output  branch_taken,

  input [15:0] reg_read_data1,
  input [15:0] reg_read_data2,

  output reg[2:0] alu_cmd,
  output reg [2:0] id_op_dest,
  output [2:0] src1,
  output [2:0] src2,
  output reg [15:0] operand1,
  output reg [15:0] operand2,
  output reg [15:0] id_mem_store_data,
  output reg id_mem_write_enable,

  output reg id_wb_mux,
  output reg id_wb_en,
  output reg op_code_is_ld,
  output reg op_code_is_st,
  output reg [2:0] id_src1, id_src2
  );
  parameter NOP = 0;
  parameter ADDI = 9;
  parameter LD = 10;
  parameter ST = 11;

	wire [3:0] op_code = instruction[15:12];
  wire [5:0] imm;
  assign src1 = (op_code == NOP)? 3'b0: instruction [8:6]; // ST, LD => Base
	assign src2 = (op_code == NOP || op_code == ADDI || op_code == LD)? 3'b0:
                ((op_code == ST)? instruction[11:9]:instruction [5:3]);

	//assign alu_cmd = (op_code < 9) ? op_code[2:0] -1: 0;//ALU default action = ADD
  assign imm = instruction[5:0];

  reg branch_taken_reg;
  assign branch_taken = ((op_code == 12) && (reg_read_data1 == 0)) ? 1 : 0;
  assign branch_offset_imm = imm;


  always@(posedge clk, posedge rst)
	begin
		if(rst)
			begin
        ex_pc <= 0;
        {alu_cmd, id_mem_write_enable, id_op_dest, id_wb_en, id_wb_mux} <= 0;
        {operand1, operand2} <= 0;
        {id_src1, id_src2} <= 0;
        id_mem_store_data <= 0;
        {op_code_is_ld,op_code_is_st} <= 0;
      end
		else begin
      if(!freeze) begin
        ex_pc <= 0;
  			{alu_cmd, id_mem_write_enable, id_op_dest, id_wb_en, id_wb_mux, id_mem_store_data} <= 0;
  			{operand1, operand2} <= 0;
        {id_src1, id_src2} <= 0;
        {op_code_is_ld,op_code_is_st} <= 0;
        if(instruction_decode_en) begin
    			branch_taken_reg <= branch_taken;
    			if(!branch_taken && !branch_taken_reg && op_code != NOP) begin
            ex_pc <= id_pc;
            if(op_code == LD)
              op_code_is_ld <= 1'b1;
            if(op_code == ST)
              op_code_is_st <= 1'b1;
            id_src1 <= src1;
            id_src2 <= src2;
    				id_op_dest <= (op_code != ST) ? instruction[11:9] : 0;
            if (op_code == ST)
              id_mem_store_data <= reg_read_data2;
            operand1 <= reg_read_data1;
            if(op_code == ADDI || op_code == LD || op_code == ST)
              operand2 <= $signed(imm);
            else
              operand2 <= reg_read_data2;

    				if(op_code < 9) //RTYPE
    					alu_cmd <= op_code[2:0] -1;
    				else alu_cmd <= 0; // ADDI, LD, ST, BZ

    				//write back controll signals
    				if(op_code >= 1 && op_code <= 10)
    					id_wb_en <= 1;

            if(op_code == 10) // load
              id_wb_mux <= 1; // mem_data  -> regfile

            //Mem control signals
            if(op_code == 11) // store
              id_mem_write_enable <= 1;
    			end
        end
      end
		end
	end

endmodule


//enumeration encoding
//enum reg [1:0] {idle=2'b00, start=2'b01, wait=2'b10, stop = 2'b11} states;
