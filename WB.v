module WB_stage
(
	// from EX stage
	input mem_wb_mux,
	input mem_wb_en,
	input [2:0] mem_op_dest,
	input [15:0] mem_alu_res,
	input [15:0] mem_mem_data,
	// to register file
	output			wb_write_en,
	output [2:0]	wb_write_dest,
	output [15:0]	wb_write_data
	// to hazard detection unit
	//output [2:0]	wb_op_dest
);

	assign wb_write_en = mem_wb_en;
	assign wb_write_data = mem_wb_mux ?  mem_mem_data: mem_alu_res;
	assign wb_write_dest = mem_op_dest;

endmodule
