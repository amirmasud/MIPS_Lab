module Hazard_detection (input [2:0] src1, src2, id_dest, ex_op_dest, mem_op_dest,
                          input id_op_code_is_ld, ex_op_code_is_mem, ready,
                          output reg stall, output reg freeze);

        always@(*) begin
          stall <= 0;
          freeze <= ex_op_code_is_mem & ~ready; // is it correct?

          if(src1 != 0) begin
            if(id_op_code_is_ld && src1 == id_dest)
              stall <= 1;
          end
          if(src2 != 0) begin
            if(id_op_code_is_ld && src2 == id_dest)
              stall <= 1;
          end

          //   if((src1 != 0 &&
  				//  (id_dest == src1 || ex_op_dest == src1 || mem_op_dest == src1)) ||
          //      (src2 != 0 &&
  				//  (ex_op_dest == src2 || id_dest == src2 || mem_op_dest == src2))
  				//  )
          // stall <= 1;
        end
endmodule
